# t3/cms

**Extended meta package for Composer/Packagist.**

Extended [t3/cms](https://packagist.org/packages/t3/cms) which uses 
[typo3/minimal](https://packagist.org/packages/typo3/minimal) meta package, 
including [typo3_console](https://packagist.org/packages/helhum/typo3-console).


## Packages

The following packages are **required**:

- typo3/minimal
- typo3/cms-cli
- typo3/cms-fluid-styled-content
- typo3/cms-rte-ckeditor *(for 8.7 and higher)*
- typo3/cms-belog
- typo3/cms-beuser
- typo3/cms-info
- typo3/cms-info-pagetsconfig *(for 7.6 and 8.7 only)*
- typo3/cms-lowlevel
- typo3/cms-setup
- typo3/cms-tstemplate
- helhum/typo3-console

**Extended:**

- typo3/cms-form   
- typo3/cms-adminpanel
- typo3/cms-felogin
- typo3/cms-redirects
- typo3/cms-seo    
- typo3/cms-t3editor
- typo3/cms-impexp 
- t3/save          
- t3/min           
- lochmueller/staticfilecache


## Installation

Just add **t3/cms** as requirement to your composer.json. The following
versions are available:

- `^9.5` / `dev-master`

**Example:**
```
$ composer require t3/cms-extended:"^9.5"  
```
